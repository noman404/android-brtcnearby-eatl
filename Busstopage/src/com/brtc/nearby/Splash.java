package com.brtc.nearby;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

public class Splash extends Activity {

	public DBAdapter db;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		new AsyncLoadDB().execute();
	}

	public class AsyncLoadDB extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			db = new DBAdapter(Splash.this);
			db.open();
			try {
				Thread.sleep(1500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				return null;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			startActivity(new Intent(Splash.this, MainActivity.class));
			finish();
		}

	}
}
