package com.brtc.nearby;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DBHelper extends SQLiteOpenHelper {

	public DBHelper(Context context) {
		super(context, StaticVars.DB_NAME, null, StaticVars.DB_VER);

	}

	// database table-column declaration
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE bus_stopages("
				+ "_id INTEGER PRIMARY KEY AUTOINCREMENT," + " name TEXT,"
				+ " location TEXT," + " latitude TEXT," + " longitude TEXT)");

		PRE_DEFINED_VALUES(db);
	}

	// pre defined values insertion
	private void PRE_DEFINED_VALUES(SQLiteDatabase db) {
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('Badda','Dhaka','23.7902121','90.4257357')");
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('Gabtali','Dhaka','23.782613','90.342775')");
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('Mohammadpur','Dhaka','23.7645786','90.35977')");
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('Gazipur','Dhaka','24.1109877','90.4258489')");
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('Mirpur-12','Dhaka','23.828857','90.364685')");
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('Balughat (cantonment)','Dhaka','23.8295261','90.3911304')");
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('Gulshan-2','Dhaka','23.7968207','90.4142189')");
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('Nabinagar','Dhaka','23.8870928','90.9630417')");
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('Rupnagar(Duaripara Bus stand)','Dhaka','23.8226581','90.3546085')");
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('Khilgaon','Dhaka','23.747998','90.423628')");
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('Shahabag','Dhaka','23.7403393','90.3941024')");
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('Mohakhali','Dhaka','23.7788761','90.4047181')");
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('Shaheen college Bus stop','Dhaka','23.7756496','90.3918439')");
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('banani South Bus Stop','Dhaka','23.7946979','90.405896')");
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('Sainik club Bus stop','Dhaka','23.7843308','90.4036214')");
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('Kuril Chowrasta Bus','Dhaka','stop 23.8172746','90.4214742')");
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('Kuril Bishwa road','Dhaka','23.8172746','90.4214742')");
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('Natun Bazar Bus stand','Dhaka','23.7932057','90.4203155')");
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('Shahjadpur Bus Stand','Dhaka','23.7932057','90.4203155')");
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('Uttar badda','Dhaka','23.7932057','90.4203155')");
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('Madhya badda','Dhaka','23.7826815','90.4219892')");
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('TB gate Bus Stand','Dhaka','23.7802126','90.4094043')");
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('Tejturi bazar Bus stand','Dhaka','23.7587838','90.3900562')");
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('Khejur bagan bus stop','Dhaka','23.7588826','90.3835485')");
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('Khamar bari','Dhaka','23.7588826','90.3835485')");
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('Asad gate','Dhaka','23.7603064','90.373914')");
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('Manik mian Avenue West Bus stop','Dhaka','23.7603064','90.373914')");
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('Town Hall Mohammudpur','Dhaka','23.760061','90.366744')");
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('Asad gate','Dhaka','23.7579783','90.373823')");
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('Farmgate (police box)','Dhaka','23.7559919','90.3877521')");
		db.execSQL("INSERT INTO bus_stopages(name, location, latitude, longitude) VALUES ('Farmgate bus stop','Dhaka','23.7559919','90.3877521')");
	}

	// update table - dropping and renewing
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS bus_stopages");
		onCreate(db);

	}
}
