package com.brtc.nearby;

import java.util.ArrayList;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends FragmentActivity implements LocationListener {

	public GoogleMap googleMap;
	public Double lat = 0.0, lng = 0.0, busLat, busLng;
	public ArrayList<HashMap<String, String>> mapList = new ArrayList<HashMap<String, String>>();
	public HashMap<String, String> mapLatLong = new HashMap<String, String>();;
	public String busStoppage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map_activity);

		// Getting Google Play availability status
		int status = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(getBaseContext());

		busLat = getIntent().getDoubleExtra(StaticVars.LAT, 0.0);
		busLng = getIntent().getDoubleExtra(StaticVars.LONG, 0.0);

		// Toast.makeText(MapActivity.this, mlat + ", " + mlng,
		// Toast.LENGTH_SHORT)
		// .show();
		busStoppage = getIntent().getStringExtra(StaticVars.BUS_STOPPAGE);

		// Showing status
		if (status != ConnectionResult.SUCCESS) { // Google Play Services are
													// not available
			int requestCode = 10;
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this,
					requestCode);
			dialog.show();

		} else { // Google Play Services are available

			// Getting reference to the SupportMapFragment of activity_main.xml
			SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.map);

			// Getting GoogleMap object from the fragment
			googleMap = fm.getMap();

			// Enabling MyLocation Layer of Google Map
			googleMap.setMyLocationEnabled(true);
			// Getting LocationManager object from System Service

			// LOCATION_SERVICE
			LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

			// Creating a criteria object to retrieve provider
			Criteria criteria = new Criteria();

			// Getting the name of the best provider
			String provider = locationManager.getBestProvider(criteria, true);

			// Getting Current Location
			Location location = locationManager.getLastKnownLocation(provider);
			googleMap.setOnMyLocationChangeListener(myLocationChangeListener);

			LatLng latLng = new LatLng(busLat, busLng);
			googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
			googleMap.animateCamera(CameraUpdateFactory.zoomTo(10));

			drawMarker();
		}
	}

	@SuppressLint("ShowToast")
	private void drawMarker() {
		googleMap.clear();

		googleMap.addMarker(new MarkerOptions()
				.position(new LatLng(busLat, busLng)).title(busStoppage));
		googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(busLat, busLng)));
		if (lat != 0 && lng != 0) {
			googleMap.addMarker(new MarkerOptions().position(new LatLng(lat,
					lng)));
			
			
			
		}
	}

	public void myClick(View v) {

		if (lat != 0 && lng != 0) {
			Toast.makeText(getApplicationContext(), "" + lat + ", " + lng,
					Toast.LENGTH_SHORT).show();
		} else {
			Toast.makeText(getApplicationContext(), "Nothing",
					Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onLocationChanged(Location location) {

		// Getting latitude of the current location
		double latitude = location.getLatitude();
		lat = latitude;

		// Getting longitude of the current location
		double longitude = location.getLongitude();
		lng = longitude;
		// Creating a LatLng object for the current location
		LatLng latLng = new LatLng(latitude, longitude);

		// Showing the current location in Google Map
		googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

		// Zoom in the Google Map
		googleMap.animateCamera(CameraUpdateFactory.zoomTo(10));
		// drawMarker();

		drawMarker();

	}

	private GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
		@Override
		public void onMyLocationChange(Location location) {
			lat = location.getLatitude();
			lng = location.getLongitude();
			if (googleMap != null) {
				drawMarker();
			}
		}
	};

	@Override
	public void onProviderDisabled(String provider) {
	}

	@Override
	public void onProviderEnabled(String provider) {
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	}

	

}